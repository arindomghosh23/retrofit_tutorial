import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_retrofit_tutorial/state/post_store.dart';
import 'package:flutter_retrofit_tutorial/ui/home_screen.dart';
import 'package:flutter_retrofit_tutorial/ui/single_post_page.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import 'data/create_rest_cliet.dart';

class RouteGenerator {
  static Route<dynamic> generateroute(RouteSettings settings) {
    switch (settings.name) {
      case HomeScreen.route:
        return MaterialPageRoute(
          builder: (context) => _createHomeRoute(),
        );
      case SinglePostPage.route:
        {
          int params = settings.arguments;
          if (params != null) {
            return MaterialPageRoute(
                builder: (context) => SinglePostPage(
                      postId: params,
                    ));
          } else
            return _errorRoute();
        }
    }
    return MaterialPageRoute(
      builder: (context) => _createHomeRoute(),
    );
  }

  static StatefulWidget _createHomeRoute() {
    return Injector(
      inject: [
        Inject<PostStore>(
          () => PostStore(CreateRestClient.createClient()),
          joinSingleton: JoinSingleton.withCombinedReactiveInstances,
        ),
      ],
      builder: ((_) => HomeScreen()),
    );
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(
      builder: (context) => Scaffold(
        appBar: AppBar(
          title: Text("Error"),
        ),
        body: Center(
          child: Text("Error"),
        ),
      ),
    );
  }
}
