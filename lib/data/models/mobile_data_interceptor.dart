import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
class MobileDataInterceptor implements InterceptorsWrapper {
  @override
  Future onRequest(RequestOptions options) async {
    final connectivityResult = await Connectivity().checkConnectivity();
    final isMobile = connectivityResult == ConnectivityResult.mobile;
    final isLargeFile = options.uri.toString().contains(RegExp(r'(/large|/video|/posts)'));

    if (isMobile && isLargeFile) {
      throw MobileDataCostException();
    }
    return options;
  }


  @override
  Future onError(DioError err) async{

    return err;
  }

  @override
  Future onResponse(Response response) async{
    // TODO: implement onResponse
    return response;
  }
}

class MobileDataCostException implements Exception {
  final message =
      'Downloading large files on a mobile data connection may incur costs';

  @override
  String toString() => message;
}
