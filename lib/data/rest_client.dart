import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'models/post_response.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: 'https://jsonplaceholder.typicode.com/')
abstract class RestClient {

  factory RestClient(Dio dio) = _RestClient;

  @GET('posts')
  Future<List<Post>> getPosts();

  @GET('posts/{id}')
  Future<Post> getPost(@Path('id') int id);

  @POST('posts')
  Future<Post> postPost(@Body() Post body);
}
