import 'package:dio/dio.dart';
import 'package:flutter_retrofit_tutorial/data/rest_client.dart';

import 'models/mobile_data_interceptor.dart';

class CreateRestClient {
  static RestClient createClient() {
    final dio = Dio()
      ..options.headers['Cache-Control'] = 'no-cache'
      ..interceptors.addAll([MobileDataInterceptor()]);
    return RestClient(dio);
  }
}
