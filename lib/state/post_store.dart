import 'package:flutter_retrofit_tutorial/data/models/post_response.dart';
import 'package:flutter_retrofit_tutorial/data/rest_client.dart';

class PostStore {
  final RestClient _restClient;

  PostStore(this._restClient);

  List<Post> _posts;
  Post _post;

  List<Post> get posts => _posts;

  Post get post => _post;

  void getPosts() async {
    _posts = await _restClient.getPosts();
  }

  void getPostById(int Id) async {
    _post = await _restClient.getPost(Id);
  }

  Future<Post> postPost(Post post) async {
    return _restClient.postPost(post);
  }
}
