import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_retrofit_tutorial/data/models/mobile_data_interceptor.dart';
import 'package:flutter_retrofit_tutorial/data/models/post_response.dart';
import 'package:flutter_retrofit_tutorial/data/rest_client.dart';
import 'package:flutter_retrofit_tutorial/state/post_store.dart';
import 'package:flutter_retrofit_tutorial/ui/single_post_page.dart';
import 'package:provider/provider.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class HomeScreen extends StatefulWidget {
  static const route = "/";

  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    _requestPosts(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Retro blog'),
      ),
      body: StateBuilder<PostStore>(
        models: [Injector.getAsReactive<PostStore>()],
        builder: (_, reactiveModel) {
          return reactiveModel.whenConnectionState(
            onIdle: () => _buildInitials(),
            onWaiting: () => _buildLoading(),
            onData: (store) {
              return _listWidget(context, store.posts);
            },
            onError: (err) => _buildError(err.toString()),
          );
        },
      ), //_buildBody(context),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _postPosts,
      ),
    );
  }

  FutureBuilder<List<Post>> _buildBody(BuildContext context) {
    return FutureBuilder<List<Post>>(
      future: Provider.of<RestClient>(context).getPosts(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return _buildError(snapshot.error.toString());
          }
          return _buildPost(context, snapshot.data);
        } else
          return _buildLoading();
      },
    );
  }

  Widget _buildError(String error) {
    return Center(
      child: Text(
        error,
        textAlign: TextAlign.center,
        textScaleFactor: 1.3,
      ),
    );
  }

  Widget _buildInitials() {
    return Center(
      child: Text(
        "please wait while we are loding your content",
        textAlign: TextAlign.center,
        textScaleFactor: 1.3,
      ),
    );
  }

  Widget _buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  void _showSnackBar(String message) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
      ),
    );
  }

  void _postPosts() async {
    final reactiveModel = Injector.getAsReactive<PostStore>();

    final newPost = Post()
      ..title = 'New Title'
      ..body = 'New body';
    reactiveModel.state.postPost(newPost).then((post) {
      _requestPosts(context);
      print(post.toJson());
    }).catchError(
      (error) => print(error.toString()),
    );
  }

  Widget _listWidget(BuildContext context, List<Post> posts) {
    return RefreshIndicator(
      onRefresh: () async {
        _requestPosts(context);
        return;
      },
      child: _buildPost(context, posts),
    );
  }

  ListView _buildPost(BuildContext context, List<Post> posts) {
    return ListView.builder(
        itemCount: posts.length,
        padding: EdgeInsets.all(8),
        itemBuilder: (context, index) {
          return Card(
            elevation: 5,
            child: ListTile(
              title: Text(
                posts[index].title,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(posts[index].body),
              onTap: () {
                _navigateToPost(context, posts[index].id);
              },
            ),
          );
        });
  }

  void _requestPosts(BuildContext context) {
    print('request posts');
    final reactiveModel = Injector.getAsReactive<PostStore>();
    reactiveModel.setState(
      (store) => store.getPosts(),
    );
  }

  void _navigateToPost(BuildContext context, int id) {
    Navigator.pushNamed(
      context,
      SinglePostPage.route,
      arguments: id,
    );
  }
}
