import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_retrofit_tutorial/data/models/post_response.dart';
import 'package:flutter_retrofit_tutorial/data/rest_client.dart';
import 'package:flutter_retrofit_tutorial/state/post_store.dart';
import 'package:provider/provider.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class SinglePostPage extends StatelessWidget {
  static const route = '/single_post';

  final int postId;

  const SinglePostPage({Key key, this.postId}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    _getPost(postId);
    return Scaffold(
      appBar: AppBar(
        title: Text('Chopper blog'),
      ),
      body: StateBuilder<PostStore>(
        models: [Injector.getAsReactive<PostStore>()],
        builder: (_, reactiveModel) {
          return reactiveModel.whenConnectionState(
            onIdle: () => _buildInitials(),
            onWaiting: () => _buildLoading(),
            onData: (state) => _buildPost(state.post),
            onError: (err) => _buildError(err.toString()),
          );
        },
      ),
    );
  }

  void _getPost(int id) {
    print('post by id');
    final reacticeModel = Injector.getAsReactive<PostStore>();
    reacticeModel.setState((store) => store.getPostById(id));
  }

  Widget _buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _buildInitials() {
    return Center(
      child: Text(
        "please wait while we are loding your content",
        textAlign: TextAlign.center,
        textScaleFactor: 1.3,
      ),
    );
  }

  Widget _buildError(String error) {
    return Center(
      child: Text(
        error,
        textAlign: TextAlign.center,
        textScaleFactor: 1.3,
      ),
    );
  }

  Widget _buildFuturePost(BuildContext context) {
    return FutureBuilder<Post>(
      future: Provider.of<RestClient>(context).getPost(postId),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return _buildPost(snapshot.data);
        } else {
          return _buildLoading();
        }
      },
    );
  }

  Widget _buildPost(Post post) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Text(
            post.title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
          SizedBox(height: 8),
          Text(post.body),
        ],
      ),
    );
  }
}
