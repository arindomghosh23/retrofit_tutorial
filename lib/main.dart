import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:flutter_retrofit_tutorial/data/create_rest_cliet.dart';
import 'package:flutter_retrofit_tutorial/route_generator.dart';
import 'package:flutter_retrofit_tutorial/state/post_store.dart';
import 'package:flutter_retrofit_tutorial/ui/home_screen.dart';
import 'package:provider/provider.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Retrofit App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: HomeScreen.route,
      onGenerateRoute: RouteGenerator.generateroute,
    );
  }
}
/*
MultiProvider(
providers: [Provider(create: (_) => CreateRestClient.createClient())],
child: MaterialApp(
title: 'Retrofit App',
theme: ThemeData(
primarySwatch: Colors.blue,
),
home: HomeScreen()),
);*/
